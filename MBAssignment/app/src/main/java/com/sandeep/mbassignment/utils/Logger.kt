package com.sandeep.mbassignment.utils

import android.util.Log

/**
 * Created by Sandeep on 31/12/17.
 */

class Logger(classname: String = "") {


    var TAG: String = "MB:" + classname;

    var method: String = ""

    fun log(message: String, error: Error? = null) {

        if (error != null) {
            if (method.length > 0)
                Log.e(TAG, method + " -> " + message, error)
            else
                Log.e(TAG, message, error)
        } else {
            if (method.length > 0)
                Log.d(TAG, method + " -> " + message)
            else
                Log.d(TAG, message)
        }
    }

    fun log(message: String, method: String = "", error: Error? = null) {

        if (error != null) {
            if (method.length > 0)
                Log.e(TAG, method + " -> " + message, error)
            else
                Log.e(TAG, message, error)
        } else {
            if (method.length > 0)
                Log.d(TAG, method + " -> " + message)
            else
                Log.d(TAG, message)
        }
    }

}

