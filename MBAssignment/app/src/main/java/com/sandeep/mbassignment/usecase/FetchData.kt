package com.sandeep.mbassignment.usecase

import com.google.gson.Gson
import com.sandeep.mbassignment.model.LessonModel
import com.sandeep.mbassignment.network.ApiClient
import com.sandeep.mbassignment.network.ApiInterface
import com.sandeep.mbassignment.network.response.LessonsList
import com.sandeep.mbassignment.utils.Logger
import io.reactivex.Observable
import io.reactivex.Observer
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


/**
 * Created by Sandeep on 5/3/18.
 *
 * To fetch from server
 */
class FetchData {

    val logger = Logger("FetchData")

    fun getLesson(observer: Observer<LessonModel>) {

        val observable: Observable<LessonModel> = Observable.create<LessonModel> { emitter ->

            val apiService = ApiClient.getClient().create(ApiInterface::class.java)
            val call: Call<LessonsList> = apiService.lessonData()

            call.enqueue(object : Callback<LessonsList> {
                override fun onResponse(call: Call<LessonsList>?, response: Response<LessonsList>?) {
                    logger.log("Response - " + response.toString())
                    val lessonsList: LessonsList = response!!.body()!!
                    logger.log("Response - " + Gson().toJson(lessonsList))

                    lessonsList.lesson_data.forEach { model -> emitter.onNext(model) }

                    emitter.onComplete()

                }

                override fun onFailure(call: Call<LessonsList>?, t: Throwable?) {

                    emitter.onError(object : Throwable() {
                        override val message: String?
                            get() = "Error getting data"
                    })
                }

            })
        }

        observable.subscribe(observer)

        logger.method = "getData()"


    }

}