package com.appkarverz.rupasri.utils

import android.content.Context
import android.content.Intent
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.AppCompatEditText
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Toast

/**
 * Created by sandeep on 18/12/17.
 */
fun View.snack(message: String, length: Int = Snackbar.LENGTH_SHORT) {
    val snack = Snackbar.make(this, message, length)
    snack.show()
}

fun Context.snack(view: View, message: String, length: Int = Snackbar.LENGTH_SHORT) {
    val snack = Snackbar.make(view, message, length)
    snack.show()
}

fun Context.snack(view: View, message: Int, length: Int = Snackbar.LENGTH_SHORT) {
    val snack = Snackbar.make(view, message, length)
    snack.show()
}

fun Context.toast(message: String, length: Int = Toast.LENGTH_SHORT) =
        Toast.makeText(this, message, length).show()

fun Context.toast(message: Int, length: Int = Toast.LENGTH_SHORT) =
        Toast.makeText(this, message, length).show()

fun AppCompatEditText.error(msg: String) {
    this.setError(msg);
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {

        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            this@error.setError(null)
        }

    })
}

fun <T : AppCompatActivity> Context.launchActivity(activity: Class<T>) {
    val intent = Intent(this, activity)

    this.startActivity(intent)
}

fun Context.launchActivity(intent: Intent) {
    this.startActivity(intent)
}
