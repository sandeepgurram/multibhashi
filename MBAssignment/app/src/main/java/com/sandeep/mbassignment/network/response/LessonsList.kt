package com.sandeep.mbassignment.network.response

import com.sandeep.mbassignment.model.LessonModel

/**
 * Created by Sandeep on 5/3/18.
 */
class LessonsList(val lesson_data: ArrayList<LessonModel>)