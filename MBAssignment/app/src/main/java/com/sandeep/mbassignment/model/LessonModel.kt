package com.sandeep.mbassignment.model

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by Sandeep on 5/3/18.
 *
 **/
class LessonModel(val type: String = "",
                  val conceptName: String = "",
                  val pronunciation: String = "",
                  val targetScript: String = "",
                  val audio_url: String = "") : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(type)
        parcel.writeString(conceptName)
        parcel.writeString(pronunciation)
        parcel.writeString(targetScript)
        parcel.writeString(audio_url)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<LessonModel> {
        override fun createFromParcel(parcel: Parcel): LessonModel {
            return LessonModel(parcel)
        }

        override fun newArray(size: Int): Array<LessonModel?> {
            return arrayOfNulls(size)
        }
    }
}