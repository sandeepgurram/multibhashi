package com.sandeep.mbassignment.usecase

import android.media.MediaPlayer
import android.os.AsyncTask
import com.sandeep.mbassignment.utils.Logger
import java.lang.IllegalStateException

/**
 * Created by Sandeep on 5/3/18.
 *
 * To control audio.
 *
 * 1. Fetch audio from URL specified in background
 * 2. Play and pause the audio
 */
class PlayAudio {

    private val log = Logger("PlayAudio")

    private val mediaPlayer = MediaPlayer()
    private var downloading = true
    var prepared = false
    var playing = false

    fun init(url: String) {
        log.log("init")
        Player().execute(url)
    }

    fun toggleAudio() {
        log.log("toggleAudio")
        if (!downloading) {
            try {
                if (mediaPlayer.isPlaying) {
                    playing = false
                    mediaPlayer.stop()
                } else {
                    playing = true
                    mediaPlayer.prepare()
                    mediaPlayer.isLooping = true
                    mediaPlayer.start()
                }
            } catch (e: IllegalStateException) {
                e.printStackTrace()
            }

        }
    }

    /**
     * to release the media player
     */
    fun release() {
        log.log("release")
        if (mediaPlayer != null) {
            playing = false
            try {
                mediaPlayer.reset()
                mediaPlayer.release()
            } catch (e: IllegalStateException) {
                e.printStackTrace()
            }
        }
    }

    /**
     * Download the audio in background
     */
    inner class Player() : AsyncTask<String, Void, Boolean>() {

        override fun doInBackground(vararg p0: String?): Boolean {
            log.log("downloading in background")

            try {
                mediaPlayer.setDataSource(p0[0])
                mediaPlayer.setOnCompletionListener {
                    downloading = true;
                    mediaPlayer.stop();
                    mediaPlayer.reset();
                }

                prepared = true;

            } catch (e: Exception) {
                e.printStackTrace()
                log.log("Error downloading file")
                prepared = false;
            }

            return prepared;
        }

        override fun onPostExecute(result: Boolean?) {
            super.onPostExecute(result);
            log.log("downloading finsh")
            downloading = false
        }

        override fun onPreExecute() {
            super.onPreExecute();
        }
    }


}