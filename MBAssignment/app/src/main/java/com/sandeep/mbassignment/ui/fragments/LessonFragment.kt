package com.sandeep.mbassignment.ui.fragments

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.speech.RecognizerIntent
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.appkarverz.rupasri.utils.toast
import com.sandeep.mbassignment.R
import com.sandeep.mbassignment.model.LessonModel
import com.sandeep.mbassignment.usecase.PlayAudio
import com.sandeep.mbassignment.usecase.RecordAndVerify
import io.reactivex.SingleObserver
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.fragment_lesson.view.*


class LessonFragment : Fragment() {

    enum class TYPE {
        learn, question
    }

    private var mLesson: LessonModel? = null
    val player = PlayAudio()

    private lateinit var recorder: RecordAndVerify

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (arguments != null) {
            mLesson = arguments!!.getParcelable<LessonModel>(ARG_PARAM1)

            player.init(mLesson!!.audio_url)

            recorder = RecordAndVerify(mLesson!!.targetScript, "kn-IN")
        }
    }

    private var fragmentView: View? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        fragmentView = inflater.inflate(R.layout.fragment_lesson, container, false)

        fragmentView!!.pronunciation.text = mLesson!!.pronunciation
        fragmentView!!.conceptName.text = mLesson!!.conceptName
        fragmentView!!.targetScript.text = mLesson!!.targetScript

        toggleAudio(fragmentView!!)


        fragmentView!!.fabPlay.setOnClickListener({ l ->
            toggleAudio(fragmentView!!)
        })
        if (mLesson!!.type != TYPE.question.name) {
            fragmentView!!.fabRecord.hide()
            fragmentView!!.tv_test.visibility = View.GONE
        } else {
            fragmentView!!.tv_test.visibility = View.VISIBLE
            fragmentView!!.fabPlay.show()
            fragmentView!!.fabRecord.setOnClickListener({ l ->
                activity.toast("start speaking")
                recorder.start(this)

            })
        }


        return fragmentView
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            RecordAndVerify.REQ_CODE_SPEECH_INPUT -> {
                val result = data!!.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS)
                activity.toast(result[0])
                recorder.verify(result[0], object : SingleObserver<String> {
                    override fun onSubscribe(d: Disposable) {

                    }

                    override fun onError(e: Throwable) {

                    }

                    override fun onSuccess(t: String) {
                        fragmentView!!.tv_result.setText(t)
                        fragmentView!!.tv_result.visibility = View.VISIBLE
                    }
                })


            }
        }
    }

    override fun onStop() {
        super.onStop()
        player.release()
    }

    /**
     * To start and stop audio
     */
    private fun toggleAudio(view: View) {
        player.toggleAudio()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            view.fabPlay.setImageDrawable(
                    if (player.playing) activity.getDrawable(R.drawable.ic_stop) else activity.getDrawable(R.drawable.ic_play))
        else
            view.fabPlay.setImageDrawable(
                    if (player.playing) activity.resources.getDrawable(R.drawable.ic_stop) else activity.resources.getDrawable(R.drawable.ic_play))
    }


    companion object {

        private val ARG_PARAM1 = "param1"

        fun newInstance(param1: LessonModel): LessonFragment {
            val fragment = LessonFragment()
            val args = Bundle()
            args.putParcelable(ARG_PARAM1, param1)
            fragment.arguments = args
            return fragment
        }
    }
}
