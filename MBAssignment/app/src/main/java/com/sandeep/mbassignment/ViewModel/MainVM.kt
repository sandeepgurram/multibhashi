package com.sandeep.mbassignment.ViewModel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.sandeep.mbassignment.model.LessonModel
import com.sandeep.mbassignment.usecase.FetchData
import com.sandeep.mbassignment.utils.Flag
import com.sandeep.mbassignment.utils.Logger
import io.reactivex.Observer
import io.reactivex.disposables.Disposable

/**
 * Created by Sandeep on 5/3/18.
 */
class MainVM : ViewModel() {

    val logger = Logger("MainVM")

    val lessons = MutableLiveData<LessonModel>()

    // Notifies if loading is completed or network error occured
    val notifier = MutableLiveData<Flag>()

    private val fetchData = FetchData()

    fun updateLessons() {

        fetchData.getLesson(object : Observer<LessonModel> {
            override fun onComplete() {
                notifier.value = Flag.COMPLETED
            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: LessonModel) {
                logger.log("Lessson = " + t.conceptName)
                lessons.value = t
            }

            override fun onError(e: Throwable) {
                logger.log("Error")
                notifier.value = Flag.ERROR
            }

        })

    }


}