package com.sandeep.mbassignment.utils

/**
 * Created by sandeep on 5/3/18.
 */
enum class Flag {
    COMPLETED, ERROR
}