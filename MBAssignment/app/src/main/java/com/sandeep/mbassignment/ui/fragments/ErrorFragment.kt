package com.sandeep.mbassignment.ui.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sandeep.mbassignment.R
import com.sandeep.mbassignment.model.LessonModel

/**
 * To show error
 */
class ErrorFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? = inflater.inflate(R.layout.fragment_error, container, false)


    companion object {
        fun newInstance() = ErrorFragment()
    }
}
