package com.sandeep.mbassignment.ui

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.appkarverz.rupasri.utils.toast
import com.sandeep.mbassignment.R
import com.sandeep.mbassignment.ViewModel.MainVM
import com.sandeep.mbassignment.model.LessonModel
import com.sandeep.mbassignment.ui.fragments.CompletedFragment
import com.sandeep.mbassignment.ui.fragments.ErrorFragment
import com.sandeep.mbassignment.ui.fragments.LessonFragment
import com.sandeep.mbassignment.ui.fragments.LoadingFragment
import com.sandeep.mbassignment.utils.Flag
import com.sandeep.mbassignment.utils.Logger
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val logger = Logger("MainActivity")

    private lateinit var viewModel: MainVM
    private var loadingCompleted = false
    private var currentLesson = -1

    private val lessonsList = ArrayList<LessonModel>()


    /**
     * Observer to get notified if lesson is fetched
     */
    private val lessonsObserver = object : Observer<LessonModel> {
        override fun onChanged(t: LessonModel?) {
            lessonsList.add(t!!)
            if (lessonsList.size == 1) {
                displayNextLesson()
            }
        }
    }

    /**
     * Observer to know if any error occured or to get notified for other events
     */
    private val flagsObserver = object : Observer<Flag> {
        override fun onChanged(t: Flag?) {
            when (t) {
                Flag.COMPLETED -> loadingCompleted = true
                Flag.ERROR -> {
                    error()

                }
            }
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewModel = ViewModelProviders.of(this).get(MainVM::class.java)

        viewModel.lessons.observe(this, lessonsObserver)
        viewModel.notifier.observe(this, flagsObserver)

        viewModel.updateLessons()
        supportFragmentManager.beginTransaction().replace(R.id.container, LoadingFragment.newInstance()).commit()

    }

    /**
     * Displays lesson two user.
     *
     * Adds/Replaces fragment to view
     */
    fun displayNextLesson(view: View? = null) {


        if (currentLesson < lessonsList.size - 1) {
            currentLesson++
            val transaction = supportFragmentManager.beginTransaction()
            transaction.replace(R.id.container, LessonFragment.newInstance(lessonsList.get(currentLesson)))
                    .commitAllowingStateLoss()
        } else {

            if (loadingCompleted)
                completed()
            else {
                val transaction = supportFragmentManager.beginTransaction()
                transaction.replace(R.id.container, LoadingFragment.newInstance()).commitAllowingStateLoss()
                toast("Fetching lessons, please wait")
            }
        }

    }

    private fun completed() {
        toast("Congrats completed lesson")

        next.hide()
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, CompletedFragment.newInstance()).commitAllowingStateLoss()
    }

    private fun error() {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, ErrorFragment.newInstance()).commit()
        toast("Error getting lessons")
    }


}


