package com.sandeep.mbassignment.usecase

import android.content.ActivityNotFoundException
import android.content.Intent
import android.speech.RecognizerIntent
import com.sandeep.mbassignment.ui.fragments.LessonFragment
import com.sandeep.mbassignment.utils.Logger
import io.reactivex.Single
import io.reactivex.SingleObserver
import io.reactivex.SingleOnSubscribe
import java.text.Collator
import java.util.*


/**
 * Created by Sandeep on 5/3/18.
 *
 * 1. Record the user voice
 * 2. Verify if he said correct answer
 * 3. Check the correctness of answer
 */
class RecordAndVerify(val speech: String, val strLang: String) {


    val logger = Logger("RecordAndVerify");

    fun start(activity: LessonFragment) {

        val intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
//        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
//                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM)
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, strLang)

        try {
            activity.startActivityForResult(intent, REQ_CODE_SPEECH_INPUT)
        } catch (a: ActivityNotFoundException) {

        }

    }

    fun verify(userSpeech: String, observer: SingleObserver<String>) {
        logger.method = "verify()"

        logger.log("Result = " + userSpeech)
        logger.log("String = " + speech)

        val observable = Single.create(SingleOnSubscribe<String> { e -> e.onSuccess(compare(userSpeech)) })

        observable.subscribe(observer)


    }

    private fun compare(userSpeech: String,
                        collator: Collator = Collator.getInstance(Locale(speech))): String {

        logger.method = "compare()"

        /**
         * 1. First we compare complete strings, if they matchs we return otherwise to do future processing
         */
        if (collator.compare(userSpeech, speech) === 0) {
            logger.log("Both Strings are equal")
            return "Perfect"
        } else {
            logger.log("Both Strings are not equal")
        }

        /**
         * 2. We compare word by word and decide the matching strength of words
         */
        return calcStrength(userSpeech)

    }


    /**
     * Decides the strength/correctness
     */
    fun calcStrength(userSpeech: String): String {

        var result = "";
        val userWords = userSpeech.split(" ")
        val speechWords = speech.split(" ")
        var correctCount = 0
        var wrongCount = 0

        val collator = Collator.getInstance(Locale(speech))

        speechWords.forEachIndexed({ position, s ->
            logger.log("Checkiing for Result = " + userWords[position])
            logger.log("Checkiing for String = " + s)

            if (collator.compare(userWords[position], s) === 0) {
                correctCount++
                result = "Both Strings are equal"
            } else {
                wrongCount++
                result = "Both Strings are not equal"
            }

        })

        if (userWords.size == speechWords.size) {
            if (correctCount >= wrongCount) {
                result = "Most of them is correct"
            } else {
                if (correctCount > 0)
                    result = "Only some its is matching"
                else
                    result = "Completely incorrect"
            }
        } else if (userWords.size >= speechWords.size) {
            result = "You spoke extra words"
        } else {
            result = "Didn't complete the sentence"
        }

        logger.log(result)

        return result
    }

    companion object {
        val REQ_CODE_SPEECH_INPUT = 234
    }


}