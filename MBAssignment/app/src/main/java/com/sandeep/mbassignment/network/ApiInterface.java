package com.sandeep.mbassignment.network;

import com.sandeep.mbassignment.network.response.LessonsList;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by sandeep on 17/12/17.
 */
public interface ApiInterface {

    @GET("getLessonData.php")
    Call<LessonsList> lessonData();

}
